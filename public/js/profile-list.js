const telephone = IMask(
	document.querySelector('.add-user__input[name="tel"]'),
	{
		mask: '{8} (000) 000-00-00',
		lazy: false
	}
);

const searchInp = document.querySelector('.search-box__input');

searchInp.addEventListener('input', searchProfile);

document.querySelector('.add-user__form').addEventListener('submit', addUserHandler);
updateProfileList();

function updateProfileList()
{
	fetch('/profile/get',
	{
		method: 'POST'
	})
	.then((res) => res.json())
	.then((profiles) =>
	{
		const searchResult = document.querySelector('.search-results');
		searchResult.innerHTML = '';

		profiles.forEach(({id, full_name: fullname, tel, next_date: nextDate}) =>
		{
			const profileItem = document.createElement('li');
			profileItem.classList.add('search-results__item');
			profileItem.dataset.id = id;
			profileItem.innerHTML =
			`
				<div class="search-results__fullname"><a class="link" href="/profile/${id}" target="_blank">${fullname}</a></div>
				<div class="search-results__tel"><a class="link" href="tel:+${tel}">${tel}</a></div>
				<div class="search-results__date">${nextDate ? new Date(nextDate).toLocaleString() : 'None'}</div>
			`;

			searchResult.appendChild(profileItem);
		});

		searchProfile.bind(searchInp).call();
	})
}

function addUserHandler(event)
{
	event.preventDefault();

	const errorsEl = document.querySelector('.add-user__results');
	const fullname = this.fullname.value.trim();
	const tel = telephone.unmaskedValue.trim();

	const err = [];

	errorsEl.innerHTML = '';

	if (fullname.length < 5)
	{
		err.push('Полное имя должно иметь хотя бы 5 символов.');
	}
	if (tel.length < 11)
	{
		err.push('Телефон должен состоять из 11 цифр.');
	}

	if (err.length > 0)
	{
		errorsEl.innerHTML = `<li class="results-list__item results-list__item--error">${err.join('</li><li class="results-list__item results-list__item--error">')}</li>`;
	}
	else
	{
		addUser(fullname, tel, {method, action} = this);
	}
}

function addUser(fullname, tel, request)
{
	fetch(request.action,
		{
			method: request.method,
			headers:
			{
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({fullname, tel})
		}
	)
	.then((res) => res.json())
	.then((res) =>
	{
		const errorsEl = document.querySelector('.add-user__results');

		if (res.status == 'error')
		{
			errorsEl.innerHTML = `<li class="results-list__item results-list__item--error">${res.error_message}</li>`;
		}
		else
		{
			errorsEl.innerHTML = `<li class="results-list__item results-list__item"><a class="link" href="/profile/${res.profileId}" target="_blank">Профиль</a> успешно создан.</li>`;

			updateProfileList();
		}
	});
}

function searchProfile()
{
	const searchText = this.value.trim();
	let searchCount = 0;

	const searchResultItems = document.querySelectorAll('.search-results__item');
	searchResultItems.forEach((item) =>
	{
		const isFound = item.textContent.indexOf(searchText) !== -1;
		item.classList.toggle('search-results__item--hide', !isFound);

		if (isFound)
		{
			searchCount += 1;
		}
	});

	document.querySelector('.search-box__title span').textContent = `${searchCount}/${searchResultItems.length}`;
}
