const searchInp = document.querySelector('.search-box__input');

searchInp.addEventListener('input', searchProfile);
searchProfile.bind(searchInp).call();


new FullCalendar.Draggable(document.querySelector('.search-results'),
	{
		itemSelector: '.search-results__item',
		eventData: function(eventEl)
		{
			const name = eventEl.querySelector('.search-results__fullname').textContent;
			const tel = eventEl.querySelector('.search-results__tel').textContent;
			const id = eventEl.dataset.id;

			return {
				title: `${name} - ${tel}`,
				url: '/profile/' +id,
				color: '#DC4242',
				backgroundColor: '#252525',
				textColor: '#ACACAC'
			};
		}
	}
);

const calendar = new FullCalendar.Calendar(document.querySelector('.calendar'),
	{
		initialView: 'timeGridWeek',
		headerToolbar:
		{
			left  : 'prev,next today',
			center: 'title',
			right : 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
		},
		locale: 'ru',
		editable: false,
		droppable: true,
		nowIndicator: true,
		expandRows: true,
		eventOverlap: false,
		eventDurationEditable: false,
		navLinks: true,
		allDaySlot: false,
		height: 'calc(100vh - 56px - 30px)',
		slotDuration: '01:00:00',
		slotMinTime: '06:00:00',
		slotMaxTime: '23:00:00',
		initialEvents:
		{
			url: '/events/get',
			method: 'POST',
			color: '#DC4242',
			backgroundColor: '#252525',
			textColor: '#ACACAC'
		},
		noEventsText: 'Нет событий для отображения',
		drop: function(arg)
		{
			const date = new Date(arg.dateStr).getTime();
			const profileId = arg.draggedEl.dataset.id;

			const profileItem = [...document.querySelectorAll('.search-results__item')].find((profile) => profile.dataset.id == profileId);
			console.log('profileItem:', profileItem)

			profileItem.querySelector('.search-results__date').textContent = new Date(date).toLocaleString();

			fetch('/events/create',
				{
					method: 'POST',
					headers:
					{
						"Content-Type": 'application/json; charset=utf-8'
					},
					body: JSON.stringify(
					{
						profileId,
						date
					})
				}
			);
		},
		eventClick: function(info)
		{
			info.jsEvent.preventDefault();

			if (info.event.url)
			{
				window.open(info.event.url, 'Profile');
			}

		},
		eventAdd: function(addInfo)
		{
			console.log(addInfo);
		}
	}
);
calendar.render();

function searchProfile()
{
	const searchText = this.value.trim();
	let searchCount = 0;

	const searchResultItems = document.querySelectorAll('.search-results__item');
	searchResultItems.forEach((item) =>
	{
		const isFound = item.textContent.indexOf(searchText) !== -1;
		item.classList.toggle('search-results__item--hide', !isFound);

		if (isFound)
		{
			searchCount += 1;
		}
	});

	document.querySelector('.search-box__title span').textContent = `${searchCount}/${searchResultItems.length}`;
}
