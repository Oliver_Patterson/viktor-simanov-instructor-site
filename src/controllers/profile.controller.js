const ProfileModel = require('../models/profile.model');

class ProfileController
{
	static async createProfile(req, res, next)
	{
		const {fullname, tel} = req.body;
		const isProfileExist = await ProfileModel.isProfileExist(tel);

		if (isProfileExist)
		{
			res.json({status: 'error', error_message: `Профиль с таким телефоном уже есть.`	});
		}
		else
		{
			const profileId = await ProfileModel.createProfile(fullname, tel);

			res.json({status: 'ok', profileId});
		}
	}
	static async getProfileListData(req, res, next)
	{
		const profiles = await ProfileModel.getProfileListData();

		res.json(profiles);
	}
	static async showProfile(req, res, next)
	{
		const {profileId} = req.params;
		const profile = await ProfileModel.getProfileData(profileId);

		res.render('profile-page.view.ejs', {profile});
	}
	static renderProfileList(req, res, next)
	{
		res.render('profile-list.view.ejs');
	}
}

module.exports = ProfileController;
