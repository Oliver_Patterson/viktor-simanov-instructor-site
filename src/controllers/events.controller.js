const ProfileModel = require('../models/profile.model');

module.exports =
{
	getEvents: async (req, res, next) =>
	{
		let {start, end} = req.body;
		start = new Date(start).getTime();
		end = new Date(end).getTime();

		let profiles = await ProfileModel.getProfileListData();

		const events = profiles.reduce((events, {full_name, tel, id, info}) =>
		{
			if (info == null)
			{
				return events;
			}

			info = JSON.parse(info);

			info.forEach((data) =>
			{
				if (data.date >= start && data.date <= end)
				{
					events.push(
						{
							title: `${full_name} - ${tel}`,
							url: '/profile/' +id,
							start: data.date
						}
					);
				}
			})

			return events;
		}, [])

		res.json(events);
	},
	createEvent: (req, res, next) =>
	{
		const {profileId, date} = req.body;

		ProfileModel.addProfileEvent(profileId, date);

		res.status(200).send('ok');
	}
};
