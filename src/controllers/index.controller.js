const ProfileModel = require('../models/profile.model');

module.exports =
{
	renderPage: async (req, res, next) =>
	{
		const profiles = await ProfileModel.getProfileListData();

		res.render('index.view.ejs', {profiles});
	}
};
