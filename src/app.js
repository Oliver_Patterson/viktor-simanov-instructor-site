require('dotenv').config();

const createError     = require('http-errors');
const express         = require('express');
const path            = require('path');
const cookieParser    = require('cookie-parser');
const logger          = require('morgan');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, '/views'));
app.set('view engine', 'ejs');

app.use(logger('short'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());


app.use('/', express.static(path.join(__dirname, '..', 'public')));


app.use('/events', require('./routes/events.router'));
app.use('/profile', require('./routes/profile.router'));
app.use('/', require('./routes/index.router'));


// catch 404 and forward to error handler
app.use((req, res, next) =>
{
	next(createError(404));
});

// error handler
app.use((err, req, res, next) =>
{
	res.locals.errorStatus = err.status || 500;

	res.status(res.locals.errorStatus).send(`Error ${res.locals.errorStatus}<br>Stack:<br><pre>${err.stack}</pre>`);
});


module.exports = app;
