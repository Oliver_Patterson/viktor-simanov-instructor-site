const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('database/database.sqlite3');

class ProfileModel
{

	static async addProfileEvent(id, date)
	{
		const curDate = new Date().getTime();
		let {info} = await ProfileModel.getProfileData(id);

		if (info == null)
		{
			info = [];
		}
		else
		{
			info = JSON.parse(info);
		}

		info.push({date, offend: []});
		info.sort((a, b) => b.date - a.date);

		db.run('UPDATE profile SET info=$info,next_date=$nextDate WHERE id=$id',
		{
			$id: id,
			$info: JSON.stringify(info),
			$nextDate: info[0].date
		}, (err) =>
		{
			if (err) console.log(err);
		});
	}
	static getProfileListData()
	{
		return new Promise( (resolve, reject) =>
		{
			db.all('SELECT * FROM profile WHERE id ORDER by next_date ASC', (err, rows) =>
			{
				if (err)
				{
					reject(err);
				}
				else
				{
					resolve(rows);
				}
			});
		});
	}
	static getProfileData(profileId)
	{
		return new Promise((resolve, reject) =>
		{
			db.get('SELECT * FROM profile WHERE id=$id', {$id: profileId}, (err, row) =>
			{
				if (err)
				{
					reject(err);
				}
				else
				{
					resolve(row);
				}
			});
		});
	}
	static isProfileExist(tel)
	{
		return new Promise((resolve, reject) =>
		{
			db.get('SELECT * FROM profile WHERE tel=$tel', {$tel: tel}, (err, row) =>
			{
				if (err)
				{
					reject(err);
				}
				else
				{
					resolve(!!row);
				}
			});
		});
	}
	static createProfile(fullname, tel)
	{
		return new Promise((resolve, reject) =>
		{
			db.run('INSERT INTO profile (full_name,tel,info) VALUES ($fullname,$tel,{})', {$fullname: fullname, $tel: tel}, function (err)
			{
				if (err)
				{
					reject(err);
				}
				else
				{
					resolve(this.lastID);
				}
			});
		});
	}
}

module.exports = ProfileModel;
