const express = require('express');
const router = express.Router();
const eventsController = require('../controllers/events.controller');


router.post('/get', eventsController.getEvents);
router.post('/create', eventsController.createEvent);


module.exports = router;
