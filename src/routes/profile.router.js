const express = require('express');
const router = express.Router();
const ProfileController = require('../controllers/profile.controller');


router.get('/', ProfileController.renderProfileList);
router.get('/:profileId', ProfileController.showProfile);

router.post('/get', ProfileController.getProfileListData);
router.post('/add', ProfileController.createProfile);


module.exports = router;
